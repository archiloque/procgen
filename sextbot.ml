type noun = R of string * string | V of string * string | M of string | Mv of string | Pn of string | Pnv of string
let sn n = R(n, n^"s")
let nouns = [|
	R("pussy", "pussies"); sn "cock"; R("penis", "penises"); sn "representative";
	R("minority","minorities"); sn "dick"; sn "breast"; sn "mouth"; sn "face";
	M "hair"; Pn "politics"; sn "soul"; sn "ear"; sn "nose"; sn "senator"; sn "kink";
	sn "girl"; sn "boy"; R("city","cities"); R("titty","titties"); sn "vagina";
	sn "butt"; R("foot","feet"); R("moose","moose"); sn "daughter"; sn "cunt";
	sn "Bolshevik"; sn "goat"; sn "tentacle"; sn "uncle"; sn "queen"; sn "peer"; 
	R("party","parties"); R("cooch","cooches"); R("snooch","snooches"); sn "tit";
	R("ass","asses"); sn "gender"; sn "cop"; M "police"; sn "loser"; sn "king";
	sn "rifle"; sn "vibrator"; Pn "authorities"; R("box","boxes"); sn "orifice";
	sn "count"; sn "baron"; R("countess","countesses"); R("baroness","baronesses");
	R("thesis","theses"); M "praxis"; M "rope"; Pn "handcuffs"; sn "boob"; 
	sn "critique"; sn "slave"; sn "buttslut"; sn "idea"; sn "revolver"; sn "corpse";
	R("ideology","ideologies");  sn "beast"; sn "youth"; sn "tail"; sn "bulge";
	sn "tty"; sn "dongle"; sn "shell"; sn "result"; sn "argument"; sn "guitar"; 
	R("class","classes"); sn "advance"; sn "restraint"; sn "nipple"; sn "whip"; 
	sn "orb"; M "junk"; sn "array"; R("matrix","matrices"); sn "crack"; sn "fork"; 
	R("mistress","mistresses"); sn "assignment"; R("switch","switches"); sn "sin";
	sn "submission"; sn "strap"; sn "noun"; sn "verb"; R("cross","crosses");
	M "faith"; Pn "beliefs"; Pn "panties";	 sn "implement";  sn "creep"; sn "Pope"; 
	sn "text"; sn "book"; sn "grimoire"; sn "twink"; sn "blog"; sn "scandal"; 
	Pn "drugs"; M "evil"; M "good"; sn "moron"; sn "fuck"; sn "neck"; sn "cad"; 
	sn "fucker"; sn "stalker"; R("beauty","beauties"); sn "jock"; sn "people";
	sn "bro"; sn "president"; sn "nerd"; sn "rival"; sn "hacker"; sn "johnson"; 
	sn "illusion"; sn "hand"; R("ash","ashes"); M "love"; sn "father"; sn "scepter";
	sn "mother"; R("boundary","boundaries"); sn "callout"; sn "appendage"; 
	sn "callout post";  sn "death"; R("witch","witches"); sn "limb"; sn "loophole";
	sn "attack surface"; sn "claw"; M "democracy"; R("woman","women");
	R("man","men"); sn "blog post"; R("knife","knives"); sn "moon";
	R("fetish","fetishes"); R("tummy","tummies"); sn "bunker";
|]
type word = A of string | An of string
let adjs = [|
	A "gleaming"; A "bleeding"; A "stupid"; A "useless"; A "sexy";
	A "tight"; A "wet"; A "hard"; A "kinky"; A "pervy"; A "Republican";
	A "naughty"; A "filthy"; A "slutty"; A "revolutionary"; A"nuclear";
	A "counterrevolutionary"; A "hopeless"; A "satanic"; A "sex";
	A "nuclear sex"; A "lusty"; A "noble"; A "witty"; A "hot";
	A "steamy"; A "Marxist"; A "horny"; A "slutty"; A "lustful";
	A "lawful";  A "bold"; A "sophisticated"; A "winsome"; A "dumb";
	A "bulging"; A "throbbing"; A "scrawny"; A "problematic";
	A "duly appointed"; A "wondrous"; A "scandalous"; A "papal";
	A "holy"; A "whimpering"; A "Bluetooth-enabled"; A "fuckable";
	A "sentient"; A "Turing-complete"; A "redneck"; A "corrective";
	A "lawless"; A "lunar"; A "solar"; A "luminous";
	
	An "awful"; An "electronic"; An "electric"; An "ugly";
	An "illegal"; An "unlawful"; An "arcane"; An "undulating";
	An "appropriate"; An "inappropriate"; An "appropriative";
	An "eminent"; An "ominous";
|]
let pick (r : 'a array) : 'a = r.(Random.int(Array.length r))
let coin () = if Random.int 2 = 0 then true else false
let np () : string * bool = let noun = pick nouns in
	let plural = match noun with 
		| Mv (_) | M(_) -> false
		| Pn (_) | Pnv(_) -> true
		| _ -> coin() in
	let form, vl = match noun with
		| V(sg,pl) -> if plural then pl,true else sg,true
		| R(sg,pl) -> if plural then pl,false else sg,false
		| Mv(n) -> n,true
		| M(n) -> n,false 
		| Pn(n) -> n,true 
		| Pnv(n) -> n,true in
	let dets = Array.append [| "my"; "your"; "the"; "some"; |]
		(if plural then [| "these"; "those"; |] else [| "this"; "that"; |]) in
	let adj = if coin () then None else Some (pick adjs) in
	(if Random.int 5 = 0 then
		if plural then form else match adj with
		| None -> if vl then "an "^form else "a "^form
		| Some(A(n)) -> "a "^n^" "^form
		| Some(An(n)) -> "an "^n^" "^form
	else (pick dets) ^(
		match adj with
			| None -> ""
			| Some(A(n)) | Some(An(n)) -> " "^n
	)^" "^form),plural;;

let strn () = let n,_ = np() in n

type form = Bare | Sg | Ppl | Ing
type verb = (form -> string option -> string)
let svb (sg,pl,ing,ppl) : verb = (fun f obj -> let o = match obj with
		| None -> ""
		| Some x -> " "^x
	in (match f with
		| Bare -> sg
		| Sg -> pl
		| Ppl -> ppl
		| Ing -> ing)^o^(if Random.int 5 != 0 then "" else" to death"))
let phrvb (sg,pl,ing,ppl) prep = (fun f obj -> let o = match obj with
		| None -> ""
		| Some x -> " "^x
	in (match f with
		| Bare -> sg
		| Sg -> pl
		| Ppl -> ppl
		| Ing -> ing)^o^" "^prep^(if Random.int 5 != 0 then "" else" to death"))
let ppvb (sg,pl,ing,ppl) prep = (fun f obj -> let o = match obj with
		| None -> ""
		| Some x -> " "^x
	in (match f with
		| Bare -> sg
		| Sg -> pl
		| Ppl -> ppl
		| Ing -> ing)^o^(if Random.int 5 != 0 then "" else" to death")^
		" "^prep^" "^(strn()))
let svt x = (x,x^"s",x^"ing",x^"ed")
let svte x = (x^"e",x^"es",x^"ing",x^"ed")
let evt x = (x,x^"es",x^"ing",x^"ed")

let trverbs : verb array = [|
	phrvb (svt "fuck") "up";
	phrvb (evt "mess") "up";
	phrvb ("hold","holds","holding","held") "down";
	phrvb (evt "push") "down";
	phrvb (svt "knock") "down";
	phrvb ("tie","ties","tying","tied") "down";
	phrvb ("tie","ties","tying","tied") "up";
	ppvb ("tie","ties","tying","tied") "up with";
	ppvb ("tie","ties","tying","tied") "to";
	phrvb ("take","takes","taking","taken") "offline";
	ppvb ("take","takes","taking","taken") "out of";
	phrvb ("take","takes","taking","taken") "out";
	ppvb ("take","takes","taking","taken") "out for";
	phrvb ("beat","beats","beating","beaten") "up";
	phrvb ("beat","beats","beating","beaten") "off";
	ppvb ("beat","beats","beating","beaten") "up with";
	ppvb (svte "shov") "onto";
	ppvb (svte "shov") "off";
	ppvb (svte "shov") "out of";
	svb (svt "fuck");
	svb (svt "sext");
	svb (svt "spank");
	svb (svt "smack");
	ppvb (svt "smack") "in";
	ppvb (svt "smack") "right in";
	ppvb (svt "smack") "with";
	ppvb ("slap", "slaps", "slapping", "slapped") "with";
	ppvb ("slap", "slaps", "slapping", "slapped") "in";
	ppvb ("slap", "slaps", "slapping", "slapped") "right in";
	phrvb ("slap", "slaps", "slapping", "slapped") "silly";
	svb ("slap", "slaps", "slapping", "slapped");
	svb (evt "publish");
	svb (evt "punish");
	ppvb (evt "punish") "with";
	svb (evt "kiss");
	ppvb (evt "kiss") "on";
	svb ("laugh at","laughs at","laughing at","laughed at");
	svb ("vore","vores","voring","vored");
	svb ("bite","bites","biting","bitten");
	svb (evt "dox");
	phrvb (svt "connect") "to the internet";
	ppvb (svt "connect") "to";
	ppvb (svt "disconnect") "from";
	phrvb (svt "disconnect") "from the internet";
	ppvb ("put","puts","putting","put") "in";
	svb ("shoot","shoots","shooting","shot");
	svb ("shoot at","shoots at","shooting at","shot at");
	phrvb ("shoot","shoots","shooting","shot") "down";
	ppvb ("shoot","shoots","shooting","shot") "in";
	svb ("police", "polices", "policing", "policed");
	svb (svt "inspect");
	svb ("defile", "defiles", "defiling", "defiled");
	svb (svt "stay");
	svb (svt "laser");
	svb (svt "censor");
	svb (svt "tweet");
	svb ("tweet at","tweets at","tweeting at","tweeted at");
	svb ("@", "@s", "@'ing", "@'ed");
	phrvb ("set","sets","setting","set") "on fire";
	phrvb ("set","sets","setting","set") "to 'stun'";
	phrvb ("get","gets","getting","gotten") "all over";
	ppvb ("get","gets","getting","gotten") "all over";
	phrvb ("strap","straps","strapping","strapped") "down";
	ppvb ("strap","straps","strapping","strapped") "to";
	phrvb ("keep","keeps","keeping","kept") "out";
	ppvb ("keep","keeps","keeping","kept") "in";
	phrvb ("keep","keeps","keeping","kept") "down";
	ppvb ("keep","keeps","keeping","kept") "out of";
	ppvb ("beg","begs","begging","begged") "for";
	ppvb (svte "forc")  "into";
	ppvb ("ship","ships","shipping","shipped") "with";
	svb ("ship","ships","shipping","shipped");
	svb ("stun","stuns","stunning","stunned");
	svb (svte "mutilat");
	svb (svte "criticiz");
	svb ("debunk","debunks","debunking","debunked");
	svb (svte "critiqu");
	svb ("smite", "smites", "smiting", "smitten");
	svb ("verify", "verifies", "verifying", "verified");
	svb ("submit", "submits", "submitting", "submitted");
	svb ("submit to", "submits to", "submitting to", "submitted to");
	svb ("conjugate", "conjugates", "conjugating", "conjugated");
	svb ("decline", "declines", "declining", "declined");
	svb ("inflect", "inflects", "inflecting", "inflected");
	ppvb ("inflict", "inflicts", "inflicting", "inflicted") "on";
	svb (svt "restrain");
	ppvb (svt "restrain") "with";
	svb ("whip", "whips", "whipping", "whipped");
	ppvb ("whip", "whips", "whipping", "whipped") "with";
	svb ("bind", "binds", "binding", "bound");
	ppvb ("bind", "binds", "binding", "bound") "with";
	ppvb ("bind", "binds", "binding", "bound") "to";
	svb (svt "respect");
	svb (svt "disrespect");
	svb (svte "dictat");
	ppvb (svte "dictat") "to";
	svb ("control", "controls", "controlling", "controlled");
	svb (svte "squeez");
	svb (svt "pull");
	svb (svt "flatten");
	svb (svt "crush");
	svb (svt "follow");
	svb (svt "unfollow");
	svb (svte "simulat");
	svb (svte "stimulat");
	svb (svte "relocat");
	ppvb (svte "relocat") "to";
	svb (svte "lik");
	svb ("split", "splits", "splitting", "split");
	svb (svte "crav");
	svb ("deny", "denies", "denying", "denied");
	svb (svt "yank");
	svb (svte "delet");
	svb (svt "compress");
	ppvb (svte "argu") "with";
	svb ("argue with", "argues with", "arguing with", "argued with");
	svb (svte "conjur");
	svb ("conjure up", "conjures up", "conjuring up", "conjured up");
	svb ("conjure upon", "conjures upon", "conjuring upon", "conjured upon");
	svb (svt "detect");
	svb ("dispel", "dispels", "dispelling", "dispelling");
	svb (svte "abus");
	ppvb (svte "disabus") "of";
	svb ("cut", "cuts", "cutting", "cut");
	ppvb ("cut", "cuts", "cutting", "cut") "with";
	ppvb ("cut", "cuts", "cutting", "cut") "up with";
	phrvb ("cut", "cuts", "cutting", "cut") "up";
	svb (svte "disassembl");
	ppvb (svte "disassembl") "with";
	svb (svt "stalk");
	svb (svt "mock");
	phrvb (svt "mock") "online";
	phrvb (svt "mock") "on the internet";
	phrvb (svt "mock") "on Twitter";
	phrvb (svt "call") "out";
	phrvb (svt "call") "names";
	svb ("sin with", "sins with", "sinning with", "sinned with");
	svb ("call out", "calls out", "calling out", "called out");
	svb ("jerk it to", "jerks it to", "jerking it to", "jerked it to");
	svb ("get off on", "gets off on", "getting off on", "got off on");
	svb (svt "exploit");
	phrvb (svt "exploit") "for sex";
	svb ("portscan", "portscans", "portscanning", "portscanned");
	svb ("bully", "bullies", "bullying", "bullied");
	svb (svte "eviscerat");
	svb (svte "throttl");
	svb (svte "strangl");
	svb (svt "slaughter");
	svb (svt "sever");
	svb (svt "dismember");
	svb (svte "dislocat");
	svb (svte "decriminaliz");
	svb (svte "legaliz");
	svb (svte "grop");
	svb (svte "vap");
	svb (svte "disparag");
	svb (svte "evolv");
	svb ("evolve into", "evolves into", "evolving into", "evolved into");
	svb ("devolve into", "devolves into", "devolving into", "devolved into");
	svb (svte "inappropriat");
	svb (svte "downvot");
	svb (svte "upvot");
	svb (svt "stab");
	svb (svt "snort");
	svb ("ban", "bans", "banning", "banned");
	svb (svte "standardiz");
	svb ("commune with", "communes with", "communing with", "communed with");
	svb ("oppress", "oppresses", "oppressing", "oppressed");
	svb ("suppress", "suppresses", "suppressing", "suppressed");
	svb ("depress", "depresses", "depressing", "depressed");
	svb (svt "sadden");
	svb (svt "devour");
	svb (svte "demoniz");
	svb (svte "curs");
	svb ("spit on", "spits on", "spitting on", "spat on");
	svb ("shit on", "shits on", "shitting on", "shat on");
	svb ("sit on", "sits on", "sitting on", "sat on");
	
|]

let trvp (f : form) : string = ((pick trverbs) f (Some (strn ())))
	(*^ (if coin() then "" else " in "^(strn()))*)
let vp (f : form) : string = ((pick trverbs) f None)
	(*^ (if coin() then "" else " in "^(strn()))*)

let titles = [| 
	"mistress"; "master"; "lady"; "lord"; "señora"; "mademoiselle"; "señor"; "monsieur";
	"daddy"; "mommy"; "slave"; "girl"; "boy"; "fucktoy"; "senator"; "fuckslave"; "manslave";
	"slut"; "buttslut"; "uncle"; "buttslave"; "professor"; "princess"; "prince"; "father";
	"friar"; "president"; "dear"; "your holiness"; "your grace"; "your excellency"; "your worship";
	"your honor"; "milady"; "milord"; "mister"; "missy"; "congressman"; "your eminence";
	"your imperial majesty"; "your majesty"; "miss"; "babe";
|]

let clause () = let n, pl = np () in
	n ^ " " ^ (trvp (if pl then Bare else Sg));;
let (^^) s c = s ^ (String.make 1 c);;
let capitalize_each s = 
	let rec loop (i : int) (o : string) (sp : bool) = 
		if i = (String.length s) then o
		else match s.[i] with
			| ' ' -> loop (i+1) (o^^' ') true
			| 'a'..'z' as c -> if sp
				then loop (i+1) (o^^(Char.uppercase_ascii c)) false
				else loop (i+1) (o^^c) false
			| _ as c -> loop (i+1) (o^^c) false
	in loop 0 "" true
let spell () = let name = (pick trverbs) Bare (Some(match pick nouns with
	| R(sg,pl) | V(sg,pl) -> pick [|sg;pl|]
	| M(n) | Mv(n) | Pn(n) | Pnv(n) -> n)) in capitalize_each name
let ivb f = pick [|
	(fun () -> (pick trverbs) f (Some "you"));
	(fun () -> (pick trverbs) f (Some "myself"));
	(fun () -> (trvp f));
|]();;
let uvb f = pick [|
	(fun () -> (pick trverbs) f (Some "yourself"));
	(fun () -> (pick trverbs) f (Some "me"));
	(fun () -> (trvp f));
|]();;
let rec tpls = [|
	(fun () -> clause());
	(fun () -> "I like to "^(ivb Bare));
	(fun () -> "I'm gonna slap you silly and call you \""^(pick titles)^"\"");
	(fun () -> "you slap me silly and call me \""^(pick titles)^"\"");
	(fun () -> "slap me silly and call me \""^(pick titles)^"\"");
	(fun () -> "you want to be slapped silly and called \""^(pick titles)^"\"");
	(fun () -> "I want to "^(ivb Bare));
	(fun () -> "I wanna "^(ivb Bare));
	(fun () -> "I'm gonna "^(ivb Bare));
	(fun () -> "don't make me "^(ivb Bare));
	(fun () -> "I force you to "^(uvb Bare));
	(fun () -> "you force me to "^(ivb Bare));
	(fun () -> "this is "^(match pick adjs with A(a) | An(a) -> a));
	(fun () -> "this is very "^(match pick adjs with A(a) | An(a) -> a));
	(fun () -> "this is deeply "^(match pick adjs with A(a) | An(a) -> a));
	(fun () -> "you have to "^(uvb Bare)^" if you want to be my "^(pick titles));
	(fun () -> "I "^(ivb Bare));
	(fun () -> "I cast "^(spell ()));
	(fun () -> "don't make me cast "^(spell ()));
	(fun () -> "I cast "^(spell ())^" on you");
	(fun () -> "don't make me cast "^(spell ())^" on you");
	(fun () -> "I cast "^(spell ())^" on myself");
	(fun () -> "you cast "^(spell ()));
	(fun () -> "you cast "^(spell ())^" on me");
	(fun () -> "you cast "^(spell ())^" on yourself");
	(fun () -> "I'm "^(ivb Ing));
	(fun () -> "I've "^(ivb Ppl));
	(fun () -> "you've "^(uvb Ppl));
	(fun () -> "you "^(uvb Bare));
	(fun () -> "look me in the eye when I "^(pick trverbs) Bare (Some "you"));
	(fun () -> "keep your eyes on "^(strn())^" while I "^(ivb Bare));
	(fun () -> "you must really want to be "^(vp Ppl)^(if coin() then "" else " by "^(strn())));
	(fun () -> "you must really want me to "^(ivb Bare));
	(fun () -> "you must really want "^(strn())^" to "^(trvp Bare));
	(fun () -> "you're gonna get "^(vp Ppl));
	(fun () -> (if coin() then "please " else "") ^ "put "^(strn())^" in "^(strn())^(if coin() then "" else ", and then "^(trvp Bare)));
	(fun () -> (trvp Bare));
	(fun () -> "please " ^ (trvp Bare));
	(fun () -> ((pick trverbs) Bare (Some "me")) ^ " and " ^ ((pick trverbs) Bare (Some "me")));
	(fun () -> (pick tpls)() ^" so I can "^(ivb Bare));
	(fun () -> (pick tpls)() ^" hard");
	(fun () -> (pick tpls)() ^" harder");
	(fun () -> (pick tpls)() ^", if that's alright");
	(fun () -> (pick tpls)() ^", if you don't mind");
	(fun () -> "um, " ^ (pick tpls)());
	(fun () -> "well, " ^ (pick tpls)());
	(fun () -> "you know, " ^ (pick tpls)());
	(fun () -> "haha, " ^ (pick tpls)());
|]
let rec mtpls = [| (* haaaaack *)
	(fun () -> (pick tpls)());
	(fun () -> (pick tpls)());
	(fun () -> (pick tpls)());
	(fun () -> (pick tpls)());
	(fun () -> (pick tpls)() ^", "^(pick titles));
	(fun () -> (pick tpls)() ^", "^(pick titles));
	(fun () -> (pick tpls)() ^", you "^(match pick nouns with R(n,_) | V(n,_) | M(n) | Mv(n) | Pn(n) | Pnv(n) -> n)); (* yeesh *)
	(fun () -> (pick tpls)() ^", you "^
		(match pick adjs with A(a) | An(a) -> a)^" "^
		(match pick nouns with R(n,_) | V(n,_) | M(n) | Mv(n) | Pn(n) | Pnv(n) -> n)); (* yeeeeeeeeeeesh *)
|]
let rec ntpls = [|
	(fun name -> (pick tpls)() ^", "^name);
	(fun name -> "Hey "^name^", "^(pick tpls)());
	(fun name -> "Hey "^name^", "^(trvp Bare));
|]
let () = Random.self_init ();
	if (Array.length Sys.argv = 1) || (Sys.argv.(1) = "") then print_string (String.capitalize_ascii ((pick mtpls)()))
	else print_string (String.capitalize_ascii ((pick ntpls) Sys.argv.(1)))
	
